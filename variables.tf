# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED PARAMETERS
# You must provide a value for each of these parameters.
# ---------------------------------------------------------------------------------------------------------------------
variable "environment" {
  default = "dev"
  type  = string
}

variable "business_unit" {
  default = "tcx"
  type = string
}

variable "subscription_type" {
  default = "TCX-LOWER"
  type = string
}

variable "application_name" {
  default = "tcx-dev-t909f1ca4c3a"
  type = string
}

variable "secret_list" {
  default     = ["mongo-db","mongo-user"]
  description = "List of users with Key Vault admin Access"
  type        = list(string)
}

# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL PARAMETERS
# These parameters have reasonable defaults.
# ---------------------------------------------------------------------------------------------------------------------

variable "admin_access_list" {
  default = ["tcx-slipway"]
  description = "List of users with Key Vault admin Access"
  type        = list(string)
}

variable "readonly_access_list" {
  default     = []
  description = "List of users with Key Vault read-only Access"
  type        = list(string)
}

variable tags {
  default     = {}
  description = "Environment tags"
  type        = map(string)
}

variable "keyvault_sp" {
  default = "77d9dcec-a0f0-47f5-a7f2-86c0180c11ed"
  type = string
}

variable "enabled_for_disk_encryption" {
  default = true
  type = bool
}

variable "enabled_for_deployment" {
  default = true
  type = bool
}

variable "enabled_for_template_deployment" {
  default = true
  type = bool
}

variable "purge_protection_enabled" {
  default = true
  type = bool
}

variable "service_principal_id" {
  default = "77d9dcec-a0f0-47f5-a7f2-86c0180c11ed"
  type = string
}

variable "default_username" {
  default = "rsubram3@eu.trimblecorp.net"
  type = string
}

variable vault_group_name {
  description = "Group name for vault users"
  type        = string
}