terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "=2.61.0"
    }
  }
}

provider "azurerm" {
  features {}
}

data "azurerm_client_config" "current" {}

data "azurerm_resource_group" "vault" {
  name = "tcxLOWER"
  location = var.location
}

locals {
  bitbucketProjectName   = lower(var.bitbucketProjectName)
  business_unit          = lower(var.business_unit)
  subscription_type      = lower(var.subscription_type)
  environment            = lower(var.environment)
  team                   = lower(var.team)
  resource_group_name    = lower(var.resource_group_name)
#  resource_group_name    = format("rg-%s-%s", local.application_name, local.subscription_type)
#  keyvault_name          = format("kv-%s-%s", local.subscription_type, local.deployment_environment)
  resource_tags = merge(
    var.tags,
    {
      "managed_by"        = "Terraform"
      "business_unit"     = var.business_unit
      "subscription_type" = var.subscription_type
      "application_name"  = var.application_name
      "team"              = var.team
      "environment"       = var.environment
    },
  )
}


resource "azurerm_key_vault" "key_vault" {
  name                            = var.application_name
  location                        = data.azurerm_resource_group.vault.location
  resource_group_name             = data.azurerm_resource_group.vault.name
  enabled_for_disk_encryption     = var.enabled_for_disk_encryption
  enabled_for_deployment          = var.enabled_for_deployment
  enabled_for_template_deployment = var.enabled_for_template_deployment
  tenant_id                       = data.azurerm_client_config.current.tenant_id
  sku_name                        = "standard"
  soft_delete_retention_days      = 90
  purge_protection_enabled        = var.purge_protection_enabled
  tags                            = local.resource_tags
}

data "azuread_service_principal" "application_resource" {
 application_id = var.service_principal_id
}

data "azuread_groups" "admin_access_list" {
  display_names = var.admin_access_list
}

resource "azuread_group" "vault_group" {
  display_name = var.vault_group_name
}

resource "azurerm_key_vault_access_policy" "admin_policy" {
  for_each     = toset(data.azuread_groups.admin_access_list.object_ids)
  key_vault_id = azurerm_key_vault.key_vault.id

  object_id = each.value
  tenant_id = data.azurerm_client_config.current.tenant_id

  key_permissions = [
    "backup",
    "create",
    "decrypt",
    "delete",
    "encrypt",
    "get",
    "import",
    "list",
    "purge",
    "recover",
    "restore",
    "sign",
    "unwrapKey",
    "update",
    "verify",
    "wrapKey",
  ]

  secret_permissions = [
    "backup",
    "delete",
    "get",
    "list",
    "purge",
    "recover",
    "restore",
    "set",
  ]

  certificate_permissions = [
    "backup",
    "create",
    "delete",
    "deleteissuers",
    "get",
    "getissuers",
    "import",
    "list",
    "listissuers",
    "managecontacts",
    "manageissuers",
    "purge",
    "recover",
    "restore",
    "setissuers",
    "update",
  ]
}

resource "azurerm_key_vault_access_policy" "terraform_service_principal" {
  key_vault_id = azurerm_key_vault.key_vault.id
  tenant_id    = azurerm_key_vault.key_vault.tenant_id
  object_id    = data.azuread_service_principal.application_resource.id
  key_permissions = [
    "create",
    "get",
    "list",
    "backup",
    "decrypt",
    "delete",
    "encrypt",
    "import",
    "recover",
    "restore",
    "sign",
    "unwrapKey",
    "update",
    "verify",
    "wrapKey"
  ]

  secret_permissions = [
    "set",
    "get",
    "delete",
    "list",
    "restore",
    "recover",
    "backup"
  ]

  certificate_permissions = [
    "backup",
    "create",
    "delete",
    "deleteissuers",
    "get",
    "getissuers",
    "import",
    "list",
    "listissuers",
    "managecontacts",
    "manageissuers",
    "recover",
    "restore",
    "setissuers",
    "update"
  ]
}

resource "azurerm_key_vault_key" "key" {
  count        = var.add_hashicorp_seal_key ? 1 : 0
  name         = "vault-seal"
  key_vault_id = azurerm_key_vault.key_vault.id
  key_type     = "RSA"
  key_size     = 2048

  key_opts = [
    "decrypt",
    "encrypt",
    "sign",
    "unwrapKey",
    "verify",
    "wrapKey",
  ]
}

resource "azurerm_key_vault_secret" "terraform_secret" {
  count        = length(var.secret_list)
  name         = var.secret_list[count.index]
  value        = var.default_username
  content_type = "username"
  key_vault_id = azurerm_key_vault.key_vault.id
  tags         = var.tags
  depends_on = [
    azurerm_key_vault.key_vault,
    azurerm_key_vault_access_policy.admin_policy,
  ]
}
