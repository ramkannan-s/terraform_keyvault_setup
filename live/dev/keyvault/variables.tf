variable "business_unit" {
  default = "tcx"
  type    = string
}

variable "subscription_type" {
  default = "c3858f8e-ebe6-4079-a815-8f4d7826f5de"
  type    = string
}

variable "application_name" {
  default     = ["tcx-dev-i","tcx-prd-i"]
  description = "List of users with Key Vault admin Access"
  type        = list(string)  
}

variable "location" {
  default = "East US 2"
  type    = string
}

variable vault_group_name {
  default     = "tcx-slipway-test"
  description = "Group name for vault users"
  type        = string
}

variable bitbucketProjectName {
  default     = "tcx"
  description = "Bitbucket project name"
  type        = string
}

variable team {
  default     = "Slipway"
  description = "team name"
  type        = string
}

variable resource_group_name {
  default     = "tcxLOWER"
  description = "resource_group_name"
  type        = string
}

variable "tags" {
  default = {}
  type    = map(string)
}

variable "keyvault_sp" {
  default = "77d9dcec-a0f0-47f5-a7f2-86c0180c11ed" #"SP-KEYVAULT"
  type    = string
}

variable "environment" {
  default = "dev"
  type    = string
}

