module "vault" {
  source                  = "../../../modules/keyvault"
  location                = var.location
  business_unit           = var.business_unit
  application_name        = var.application_name
  subscription_type       = var.subscription_type
  tags                    = var.tags
  keyvault_sp             = var.keyvault_sp
  environment             = var.environment
  vault_group_name        = var.vault_group_name
}
