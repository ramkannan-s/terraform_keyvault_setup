terraform {
  required_providers {
    azuread = {
      source  = "hashicorp/azuread"
      version = "=1.4"
    }
  }
  required_version = "=0.15.3"
  backend "azurerm" {
    key = "pcb_core.tfstate"
  }
}

provider "azuread" {
}
provider "azurerm" {
  features {}
}
