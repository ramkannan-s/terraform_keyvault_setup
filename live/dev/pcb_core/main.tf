module "core_app" {
  source               = "../../../modules/azure_ad"
  app_name             = var.app_name
  redirect_url         = var.redirect_url
  app_roles            = var.app_roles
  csr_group_name       = var.csr_group_name
  resource_access_role = var.resource_access_role
}

module "core_app_secret" {
  source            = "../../../modules/app_registration_keyvault"
  application_id    = module.core_app.azuread_application_id
  secret_name       = var.secret_name
}
