variable "app_name" {
  type    = string
  default = "PCB - CSR Portal - Digital Platform (Core API)"
}

variable "redirect_url" {
  type    = list(any)
  default = []
}

variable "app_roles" {
  default = {}
}

variable "csr_group_name" {
  default = {}
}

variable "resource_access_role" {
  type = list(string)
  // User.Read.All, Group.Read.All
  default = ["df021288-bdef-4463-88db-98f22de89214", "5b567255-7703-4780-807c-7be8301ae99b"]
}

variable "secret_name" {
  type    = string
  default = "paf.azure.oauth2.client.secret"
}

variable "key_vault_name" {
  type    = string
  default = "pcf_sso_keyvault"
}
