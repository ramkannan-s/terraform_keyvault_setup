# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED PARAMETERS
# You must provide a value for each of these parameters.
# ---------------------------------------------------------------------------------------------------------------------
variable "environment" {
  default = "dev"
  type  = string
}

variable "business_unit" {
  default = "tcx"
  type = string
}

variable "subscription_type" {
  default = "c3858f8e-ebe6-4079-a815-8f4d7826f5de"
  type = string
}

variable "application_name" {
  default     = ["tcx-dev-i","tcx-prd-i"]
  description = "List of users with Key Vault admin Access"
  type        = list(string)  
}

variable "secret_maps" {
    type = map(string)
    default = {
        "name1"= "value1"
        "aaa" = "111"
    }
}

/*variable "secret_maps" {
    type = map
    default = {
      "tcx-dev-i" = {
        "name1"  = "value1",
    },
      "tcx-prd-i" = {
        "aaa"    = "111"
    }
  }
}*/

variable "location" {
  default = "East US 2"
  type    = string
}

# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL PARAMETERS
# These parameters have reasonable defaults.
# ---------------------------------------------------------------------------------------------------------------------

variable "admin_access_list" {
  default = ["tcx-slipway-test"]
  description = "List of users with Key Vault admin Access"
  type        = list(string)
}

variable "readonly_access_list" {
  default     = []
  description = "List of users with Key Vault read-only Access"
  type        = list(string)
}

variable tags {
  default     = {}
  description = "Environment tags"
  type        = map(string)
}

variable "keyvault_sp" {
  default = "77d9dcec-a0f0-47f5-a7f2-86c0180c11ed"
  type = string
}

variable "enabled_for_disk_encryption" {
  default = true
  type = bool
}

variable "purge_protection_enabled" {
  default = false
  type = bool
}

variable vault_group_name {
  default     = "tcx-slipway-test"
  description = "Group name for vault users"
  type        = string
}

variable bitbucketProjectName {
  default     = "tcx"
  description = "Bitbucket project name"
  type        = string
}

variable team {
  default     = "Slipway"
  description = "team name"
  type        = string
}

variable resource_group_name {
  default     = "tcxLOWER"
  description = "resource_group_name"
  type        = string
}