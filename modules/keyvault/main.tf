data "azurerm_client_config" "current" {}

locals {
  json_data = jsondecode(file("${path.module}/data.json"))
}

locals {
  bitbucketProjectName   = lower(var.bitbucketProjectName)
  business_unit          = lower(var.business_unit)
  subscription_type      = lower(var.subscription_type)
  environment            = lower(var.environment)
  team                   = lower(var.team)
  resource_group_name    = lower(var.resource_group_name)
  resource_tags = merge(
    var.tags,
    {
      "managed_by"        = "Terraform"
      "business-unit"     = var.business_unit
      "subscription_type" = var.subscription_type
      "team"              = local.team
      "environment"       = local.json_data.environment_name
      "primary-contact-name" = "slipway-ug"
      "primary-contact-email" = "slipway-ug@trimble.com"
      "team-contact-email" = "slipway-ug@trimble.com"
      "product" = "tcx"
    },
  )
}

locals {
  json_data_7 = jsondecode(file("${path.module}/data7.json"))
}

output "helper_list" {
  value = local.json_data_7
}

data "azurerm_resource_group" "vault" {
  name = local.resource_group_name
}
resource "azurerm_key_vault" "key_vault" {
  count                           = length(var.application_name)
  name                            = var.application_name[count.index]
  location                        = var.location
  resource_group_name             = data.azurerm_resource_group.vault.name
  enabled_for_disk_encryption     = var.enabled_for_disk_encryption
  tenant_id                       = data.azurerm_client_config.current.tenant_id
  sku_name                        = "standard"
  soft_delete_retention_days      = 7
  purge_protection_enabled        = var.purge_protection_enabled
  tags                            = local.resource_tags
}

data "azuread_service_principal" "application_resource" {
 application_id = var.keyvault_sp
}

data "azuread_groups" "admin_access_list" {
  display_names = var.admin_access_list
}

resource "azuread_group" "vault_group" {
  display_name = var.vault_group_name
}

resource "azurerm_key_vault_access_policy" "user_policy" {
  count                         = length(var.application_name)
  key_vault_id                  = azurerm_key_vault.key_vault[count.index].id
  object_id                     = data.azurerm_client_config.current.object_id
  tenant_id                     = data.azurerm_client_config.current.tenant_id
  
  key_permissions               = ["create", "get", "list", "backup", "decrypt", "delete", "encrypt", "import", "recover", "restore", "sign", "unwrapKey", "update", "verify", "wrapKey"]
  secret_permissions            = ["set", "get", "delete", "list", "restore", "recover", "backup", "purge"]
  certificate_permissions       = ["backup", "create", "delete", "deleteissuers", "get", "getissuers", "import", "list", "listissuers", "managecontacts", "manageissuers", "recover", "restore", "setissuers", "update"]
} 

/* resource "azurerm_key_vault_access_policy" "admin_policy" {
#  key_vault_id = azurerm_key_vault.key_vault.id
  for_each                      = { for idx, v in local.helper_list: idx => v }
  key_vault_id                  = azurerm_key_vault.key_vault[count.index].id
  object_id                     = azuread_group.vault_group.object_id
  tenant_id                     = data.azurerm_client_config.current.tenant_id

  key_permissions               = ["create", "get", "list", "backup", "decrypt", "delete", "encrypt", "import", "recover", "restore", "sign", "unwrapKey", "update", "verify", "wrapKey"]
  secret_permissions            = ["set", "get", "delete", "list", "restore", "recover", "backup", "purge"]
  certificate_permissions       = ["backup", "create", "delete", "deleteissuers", "get", "getissuers", "import", "list", "listissuers", "managecontacts", "manageissuers", "recover", "restore", "setissuers", "update"]
} 

resource "azurerm_key_vault_access_policy" "terraform_service_principal" {
#  key_vault_id = azurerm_key_vault.key_vault.id
  for_each                      = { for idx, v in local.helper_list: idx => v }
  key_vault_id                  = azurerm_key_vault.key_vault[count.index].id
  tenant_id                     = azurerm_key_vault.key_vault[each.key].tenant_id
  object_id                     = data.azuread_service_principal.application_resource.id

  key_permissions               = ["create", "get", "list", "backup", "decrypt", "delete", "encrypt", "import", "recover", "restore", "sign", "unwrapKey", "update", "verify", "wrapKey"]
  secret_permissions            = ["set", "get", "delete", "list", "restore", "recover", "backup", "purge"]
  certificate_permissions       = ["backup", "create", "delete", "deleteissuers", "get", "getissuers", "import", "list", "listissuers", "managecontacts", "manageissuers", "recover", "restore", "setissuers", "update"]
}  */

resource "random_password" "password" {
  length           = 16
  special          = true
  override_special = "_%@"
}

resource "azurerm_key_vault_secret" "terraform_secret" {
 # count        = length(var.secret_list)
 # name         = var.secret_list[count.index]
 # value        = random_password.password.result
#  key_vault_id = azurerm_key_vault.key_vault.id
  count        = length(var.secret_maps)
  name         = keys(var.secret_maps)[count.index]
  value        = values(var.secret_maps)[count.index]
  key_vault_id = azurerm_key_vault.key_vault[count.index].id
  tags         = var.tags
  depends_on = [
    azurerm_key_vault.key_vault,
    azurerm_key_vault_access_policy.user_policy,
  #  azurerm_key_vault_access_policy.admin_policy
  ]
}
