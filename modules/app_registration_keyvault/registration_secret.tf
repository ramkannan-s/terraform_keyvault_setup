data "azuread_application" "app_reg" {
  application_id = var.application_id
}

resource "random_password" "password" {
  length  = 32
  special = true
}

locals {
  subscription_type      = lower(var.subscription_type)
  application_name       = lower(var.application_name)
  resource_group_name    = format("rg-%s-%s", local.application_name, local.subscription_type)
}

data "azurerm_resource_group" "vault" {
  name = local.resource_group_name
}

resource "azuread_application_password" "app_pw" {
  application_object_id = data.azuread_application.app_reg.id
  description           = var.secret_name
  value                 = random_password.password.result
  end_date              = "2112-01-01T01:00:00Z"
}


data "azurerm_key_vault" "key-vault" {
  name                = var.key_vault_name
}

resource "azurerm_key_vault_secret" "vault_secret" {
  name         = var.secret_name
  value        = azuread_application_password.app_pw.value
  key_vault_id = azurerm_key_vault.key-vault.id
}
