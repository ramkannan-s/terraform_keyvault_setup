variable application_id {
  type = string
}

variable secret_name {
  type = string
}

variable key_vault_name {
  type = string
}

